/* ALTER DATABASE base_copy
MODIFY FILE (NAME = base_copy, NEWNAME = base_work)
GO*/
USE [master]
RESTORE DATABASE [base_copy] FROM  DISK = N'base_copy_work.bak' WITH REPLACE, FILE = 1,  MOVE N'base_work' TO N'C:\tmpUpdate\base_copy2.mdf',  MOVE N'base_work_log' TO N'C:\tmpUpdate\base_copy2_log.ldf', NOUNLOAD,  STATS = 10
GO