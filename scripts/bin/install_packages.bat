rem Install OneScript
call cd %~dp0
call opm update opm
IF ERRORLEVEL 1 (
    rem  OneScript not found. Try install:
    call curl -O http://oscript.io/downloads/latest/OneScript-1.2.0-x86.exe 
    start OneScript-1.2.0-x86.exe
)
pause
rem Install is over
rem Install packages used 1Script
call opm update opm
call opm install cmdline
call opm install logos
call opm install v8runner
call opm install 1commands
call opm install progbar
call opm install ReadParams
pause